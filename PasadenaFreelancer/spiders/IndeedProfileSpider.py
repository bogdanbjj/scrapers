from scrapy.http import Request
from scrapy.spiders import Spider

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException

from PasadenaFreelancer.items import IndeedPassadenaFreelancerItem


login_email = 'wangdong8790@gmail.com'
login_pass = 'somepass'


class IndeedProfileSpider(Spider):
    name = 'in_pasadena'
    allowed_domain = ['indeed.com']
    start_urls = ['https://secure.indeed.com/account/login?service=rex&hl=en_US&co=US&cfb=2&continue=http%3A%2F%2Fwww.indeed.com%2Fr%2Fe61e7e10acdf3a89']
    start_url = ['http://www.indeed.com/resumes?q=freelancers&l=Pasadena%2C+CA&co=US&radius=10&start={}'.format(
        i) for i in range(500, 600, 50)]
    source = 'Indeed.com'
    base_url = 'https://www.indeed.com/'

    def __init__(self, *args, **kwargs):
        self.browser = webdriver.Chrome()
        self.browser.get(self.start_urls[0])
        self.login((login_email, login_pass))
        super(IndeedProfileSpider, self).__init__(*args, **kwargs)

    def login(self, credentials):
        """
        credentials is a tuple that consists of
        email and password
        """
        email = self.browser.find_element_by_id("signin_email")
        password = self.browser.find_element_by_id("signin_password")
        email.send_keys(credentials[0])
        password.send_keys(credentials[1])
        email.submit()

    def parse(self, response):
        for url in self.start_url:
            yield Request(url=url, callback=self.after_login)

    def after_login(self, response):
        self.browser.get(response.url)
        links = response.xpath("//a[@class='app_link']/@href").extract()
        links = self.browser.find_elements_by_xpath("//a[@class='app_link']")
        for link in links:
            link = link.get_attribute('href')
            # url = response.url.split('/')
            # url = url[0] + "//" + url[2] + link
            yield Request(url=link, callback=self.parse_resumes)

    def parse_resumes(self, response):
        if response.status == 301:
            print response.url
        item = IndeedPassadenaFreelancerItem()
        self.browser.get(response.url)
        try:
            profile_name = self.browser.find_element_by_id("resume-contact")
        except NoSuchElementException:
            import ipdb; ipdb.set_trace()
        location = response.xpath("//p[@class='locality']/text()").extract()
        if location:
            location = location[0]
        job_title = response.xpath("//h2[@id='headline']/text()").extract()
        if job_title:
            job_title = job_title[0]
        else:
            job_title = response.xpath("//p[@class='work_title title']/text()").extract()
            if job_title:
                job_title = job_title[0]
            print "No job title for user {}".format(response.url)
        links = response.xpath("//p[@class='link_url']/a/text()").extract()
        if len(links) > 1:
            links = '\n'.join(link for link in links)
        item['profile_name'] = profile_name.text
        item['location'] = location
        item['job_title'] = job_title
        item['source'] = self.source
        item['user_url'] = response.url
        item['links'] = links
        yield item
