import logging

import time
from scrapy.http import Request
from scrapy.spiders import Spider

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

from PasadenaFreelancer.items import UpworkPasadenaFreelancerItem

logger = logging.getLogger(__name__)


class UpworkPassadenaProfileSpider(Spider):
    name = 'up_pasadena'
    start_urls = ['https://www.upwork.com/local/united-states/california/pasadena/']
    allowed_domains = ['upwork.com']
    location = "Passadena, CA"
    source = 'Upwork.com'
    VISITED_URLS = {}
    VISITED_PROFILES = {}

    def parse(self, response):
        """Gets the categories from
            For every link in the category there will be multiple
            profiles
        """
        item = UpworkPasadenaFreelancerItem()
        categories = response.xpath("//div['o-skill-location']/a/@href").extract()
        categories = categories[:-2] # LAST ELEMENT IS [#]; remove it
        for category in categories[4:]:
            cat = category.split('/')
            cat = cat[-2]
            url = response.url + cat
            yield Request(url=url, callback=self.parse_category, meta={'item': item}, dont_filter=True)

    def parse_category(self, response):
        """Keeps track of category urls users.
            Keeps track of users with name and hours worked
        """
        if self.VISITED_URLS.get(response.url):
            logger.info("Already scraped {}".format(response.url))
            return
        else:
            self.hover(response.url)
            item = response.meta['item']
            names = response.xpath(
                "//figcaption/h3[@class='font-gotham-medium']/text()").extract()
            rates = response.xpath(
                "//div[@class='rate font-gotham-medium']/text()").extract()

            hours_billed = response.xpath("//div[@class='hours-billed col-xs-6']/text()").extract()
            location = self.location
            category = response.url.split('/')
            category = category[-2].upper()
            for i in range(len(names)):
                # KEY = name + hours
                if self.VISITED_PROFILES.get((names[i] + str(hours_billed[i]))):
                    continue
                else:
                    self.VISITED_PROFILES[(names[i] + str(hours_billed[i]))] = True
                    item['category'] = category
                    item['profile_name'] = names[i]
                    item['rates'] = rates[i].replace("\n", "").strip()
                    item['hours_billed'] = hours_billed[i]
                    item['location'] = location
                    item['source'] = self.source
                    yield item

    def hover(self, url):
        """
         Method used to 'hover' over a Javascript
         element and that loads some additional
         content.
         Still not exactly sure how i click it.
         I supose you need to get a link
         that gets exposed after the 'hover' and then click it
        """
        self.browser = webdriver.Chrome()
        self.browser.get(url)
        time.sleep(1)
        element = self.browser.find_element_by_xpath("//div[@class='profile-tile-hovered']")
        hover = ActionChains(self.browser).move_to_element(element)
        hover.click()
        self.browser.close()