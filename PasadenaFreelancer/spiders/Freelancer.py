from scrapy.http import Request
from scrapy.spider import Spider


class FreelancerProfileSpider(Spider):
    name = 'free_passadena'
    allowed_domains = ['freelancer.com']
