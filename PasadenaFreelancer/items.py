# -*- coding: utf-8 -*-
import scrapy


class AbstractItem(scrapy.Item):
    profile_name = scrapy.Field()
    job_category = scrapy.Field()  # # or skill =3d modelling
    source = scrapy.Field()
    user_job_title = scrapy.Field()
    location = scrapy.Field()  # in this case Passadena


class UpworkPasadenaFreelancerItem(scrapy.Item):
    category = scrapy.Field() # # or skill =3d modelling
    profile_name = scrapy.Field()
    source = scrapy.Field()
    user_job_title = scrapy.Field()
    rates = scrapy.Field() # price/hour
    hours_billed = scrapy.Field()
    jobs_completed = scrapy.Field()
    location = scrapy.Field() # in this case Passadena
    overview = scrapy.Field() # his description of himself
    tags = scrapy.Field() # his skill tags


class IndeedPassadenaFreelancerItem(scrapy.Item):
    profile_name = scrapy.Field()
    location = scrapy.Field()
    job_title = scrapy.Field()
    source = scrapy.Field()
    user_url = scrapy.Field()
    links = scrapy.Field()

