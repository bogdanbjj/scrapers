# -*- coding: utf-8 -*-
from PasadenaFreelancer.items import IndeedPassadenaFreelancerItem, UpworkPasadenaFreelancerItem

import xlwt


class ExcelPipeline(object):
    upwork_workspace = False
    indeed_workspace = False
    row = 6
    col = 1

    def process_item(self, item, spider):

        if isinstance(item, UpworkPasadenaFreelancerItem):
            if not self.upwork_workspace:
                self.create_upwork_workspace()
            self.insert_upwork_item(item)

        if isinstance(item, IndeedPassadenaFreelancerItem):
            if not self.indeed_workspace:
                self.create_indeed_workspace()
            self.insert_indeed_item(item)

    def create_indeed_workspace(self):
        self.wb = xlwt.Workbook()
        self.ind = self.wb.add_sheet('Indeed Profiles')
        self.ind.write(0, 0, 'Source')
        self.ind.write(0, 1, 'Location')
        self.ind.write(0, 2, 'Profile name')
        self.ind.write(0, 3, 'Job title')
        self.ind.write(0, 4, 'User link')
        self.ind.write(0, 5, 'Useful links')
        self.indeed_workspace = True

    def insert_indeed_item(self, item):
        self.ind.write(self.col, 0, item.get('source'))
        self.ind.write(self.col, 1, item.get('location'))
        self.ind.write(self.col, 2, item.get('profile_name'))
        self.ind.write(self.col, 3, item.get('job_title'))
        self.ind.write(self.col, 4, item.get('user_url'))
        self.ind.write(self.col, 5, item.get('links'))

        self.col += 1
        self.wb.save("Pasadena Profiles Indeed.xls")

    def create_upwork_workspace(self):
        self.row = 6
        self.col = 1
        self.wb = xlwt.Workbook()
        self.up = self.wb.add_sheet("Upwork Profiles")
        self.up.write(0, 0, "Category")
        self.up.write(0, 1, "Source")
        self.up.write(0, 2, "Location")
        self.up.write(0, 3, "Profile name")
        self.up.write(0, 4, "Rates")
        self.up.write(0, 5, "Hours billed")
        self.upwork_workspace = True

    def insert_upwork_item(self, item):
        self.up.write(self.col, 0, item.get('category'))
        self.up.write(self.col, 1, item.get('source'))
        self.up.write(self.col, 2, item.get('location'))
        self.up.write(self.col, 3, item.get('profile_name'))
        self.up.write(self.col, 4, item.get('rates'))
        self.up.write(self.col, 5, item.get('hours_billed'))

        self.col += 1
        self.wb.save("Pasadena Profiles Upwork.xls")